package com.library.security;

import com.library.domain.Role;
import com.library.domain.User;
import com.library.exception.UserNotFoundException;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user;
        try {
            user = userService.findByEmail(email);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        Collection<GrantedAuthority> grantedAuthorities;

        grantedAuthorities = new ArrayList<>();
        for (Role role : user.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
        }

        org.springframework.security.core.userdetails.User goodUserDetails =
                new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                        true, true, true, true, grantedAuthorities);

        return goodUserDetails;
    }
}
