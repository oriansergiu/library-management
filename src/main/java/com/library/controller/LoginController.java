package com.library.controller;

import com.library.domain.dto.EmptyJsonResponse;
import com.library.domain.dto.UserLoginDTO;
import com.library.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.nio.file.AccessDeniedException;

@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody UserLoginDTO userLoginDto) throws AccessDeniedException {
        loginService.login(userLoginDto);

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }
}
