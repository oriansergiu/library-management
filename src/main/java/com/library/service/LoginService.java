package com.library.service;

import com.library.domain.dto.UserLoginDTO;
import com.library.exception.UserNotFoundException;
import com.library.security.CustomUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;

@Service
public class LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    private CustomUserDetailsService service;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void login(final UserLoginDTO userLoginDto) throws AccessDeniedException {
        LOGGER.info("Trying to authenticate user with email={}", userLoginDto.getEmail());

        UserDetails userDetails;
        try {
            userDetails = validateUser(userLoginDto);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            securityContext.setAuthentication(authentication);

            LOGGER.info("Successfully authenticate user with email={}", userLoginDto.getEmail());
        }  catch (UserNotFoundException e) {
            LOGGER.warn("Failed to authenticate user with email={}", userLoginDto.getEmail());
            throw new AccessDeniedException("Incorrect email or password");
        }
    }

    private UserDetails validateUser(final UserLoginDTO userLoginDto) throws AccessDeniedException, UserNotFoundException {
        LOGGER.info("Trying to validate the user with email={}", userLoginDto.getEmail());
        UserDetails user = service.loadUserByUsername(userLoginDto.getEmail());

        if(user == null)
        {
            LOGGER.warn("This account doesn't exist.");
            throw new AccessDeniedException("This account doesn't exist.");
        }

        boolean isPasswordCorrect = passwordEncoder.matches(userLoginDto.getPassword(), user.getPassword());

        if (isPasswordCorrect) {
            LOGGER.info("Successfully validate the user with email={}", userLoginDto.getEmail());
            return user;
        }
        throw new UserNotFoundException();
    }
}