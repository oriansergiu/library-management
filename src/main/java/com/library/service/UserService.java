package com.library.service;

import com.library.domain.User;
import com.library.exception.UserNotFoundException;
import com.library.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public User findByEmail(final String email) throws UserNotFoundException {
        User actualUser = userRepository.findByEmail(email);
        if (actualUser == null)
        {
            throw new UserNotFoundException();
        }
        return actualUser;
    }
}
